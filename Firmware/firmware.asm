
		LIST	p = 16F684

		include	"P16F684.inc"

		; UUUU	UNUSED		0011
		; F	FCMEN		0	disabled
		; I	IESO		0	disabled
		; BB	BOREN		00	disabled
		; D	~CPD		1	disabled
		; C	~CP		1	disabled
		; M	MCLRE		0	disabled
		; P	~PWRTE		0	enabled
		; W	WDTE		0	disabled
		; OOO	FOSC		001	XT

			;  UUUUFIBBDCMPWOOO
		__CONFIG b'0011000011000001'

		errorlevel -302, -231

;//////////////////////////////////////////////////////////////////////
;// CONST
;//////////////////////////////////////////////////////////////////////

;//////////////////////////////////////////////////////////////////////
;// IF THE PCB
;// YE MAKE MORE NICE,
;// CHECK THESE NUMBERS
;// SHALL THEE, THRICE!

LOAD		equ	1		; 7219 on PORT C
CLK		equ	2
DIN		equ	5

SHDN		equ	3		; SHDN on the 3400

BTN_0		equ	1		; buttons on PORT A
BTN_1		equ	2

;//////////////////////////////////////////////////////////////////////

DECODE_MODE	equ	0x9		; 7219 control registers
INTENSITY	equ	0xA
SCAN_LIMIT	equ	0xB
SHUTDOWN	equ	0xC
DISPLAY_TEST	equ	0xF

BTN_0_MSK	equ	1<<BTN_0
BTN_1_MSK	equ	1<<BTN_1

PORTA_BTNMASK	equ	BTN_0_MSK + BTN_1_MSK

; button status bits

PRESSED		equ	0		; was just pressed
HELD		equ	1		; is currently being held down
RELEASED	equ	2		; was just released

REST_BTN	equ	0 * 2		; button assignments
LAP_BTN		equ	1 * 2

; dflags bits

RESTING		equ	7		; in resting mode timer is paused and lap counter display flashes
BLOCKED		equ	6		; buttons are blocked for a moment after any press

; timings

FREEZE		equ	d'50'		; freeze timer display at lap end/start for this many 100ths of a second
BLOCK		equ	d'50'		; block input for this many 100ths of a second after a button press
RESET_TIME	equ	d'200'		; hold rest button for this many 100ths to reset time & laps

;//////////////////////////////////////////////////////////////////////
;// RAM
;//////////////////////////////////////////////////////////////////////

		UDATA	0x20

ram_start	res	0

temp0		res	1
temp1		res	1
temp2		res	1
temp3		res	1

timer		res	6		; 6 timer digits (00:00:00) = MM:SS:HH
laps		res	4		; 4 lap digits

brightness	res	1
bdir		res	1

digits		res	8		; 8 digits 0-20
dp		res	1		; 8 decimal points (1 = on)

dflags		res	1
button_block	res	1
resetcounter	res	1
timerpause	res	1
mode		res	1		; 0 = MM.SS, 1 = M.SS.T, 2 = SS.TH

buttons		res	4		; 3 buttons, 2 bytes each

pclath_temp	res	1

ram_end		res	0

		UDATA_SHR

w_temp		res	1		;  W save in shared bank ram: banksel=? in irq and have to save W before selecting bank 0
status_temp	res	1

;//////////////////////////////////////////////////////////////////////
;// MACROS
;//////////////////////////////////////////////////////////////////////

BANK0		MACRO			; assume RP1 = 0...
		bcf	STATUS, RP0
		ENDM

BANK1		MACRO
		bsf     STATUS, RP0     ; assume RP1 = 0...
		ENDM

SWAPWF		macro reg
		xorwf reg,f
		xorwf reg,w
		xorwf reg,f
		endm

set7219		MACRO	addr,val
		movLw	addr
		movwf	temp0
		movLw	val
		call	send7219
		ENDM

;//////////////////////////////////////////////////////////////////////
;// reset/irq vectors
;//////////////////////////////////////////////////////////////////////

zero		CODE	0

		goto	start

		nop
		nop
		nop

		goto	isr

;//////////////////////////////////////////////////////////////////////
;// some things in low mem because PCL/PCLATH 8 bit shenanigans
;//////////////////////////////////////////////////////////////////////

get_dig		addwf	PCL,f		; return numeral_bitmap[w]
		retlw	b'01111110'
		retlw	b'00110000'
		retlw	b'01101101'
		retlw	b'01111001'
		retlw	b'00110011'
		retlw	b'01011011'
		retlw	b'01011111'
		retlw	b'01110000'
		retlw	b'01111111'
		retlw	b'01111011'
		retlw	b'01110111'
		retlw	b'00011111'
		retlw	b'01001110'
		retlw	b'00111101'
		retlw	b'01001111'
		retlw	b'01000111'
		retlw	b'00000001'
		retlw	b'00110111'
		retlw	b'00111011'
		retlw	b'00000000'

get_dp_mask	addwf	PCL,f
		retlw	b'00000000'
		retlw	b'10000010'
		retlw	b'00010000'

;//////////////////////////////////////////////////////////////////////
;// ISR
;//////////////////////////////////////////////////////////////////////

isr		movwf	w_temp				; save state in shared ram
		swapf	STATUS, w			; get status in W
		clrf	STATUS				; now we're in bank0
		movwf	status_temp			; save status in bank0
		movf	PCLATH, w
		movwf	pclath_temp			; save pclath in bank0
		clrf	PCLATH				; clear high bits of PC before any gotos, calls etc
		btfss	PIR1,TMR2IF			; if timer2 irq
		goto	notimer2
		bcf	PIR1,TMR2IF			; 	clear it
		call	read_btns			; 	read_btns()
		btfss	dflags,RESTING			; 	if !resting
		call	do_timer			;		increment_timer()

		bcf	dflags,BLOCKED			;	dflags.BLOCKED = false
		movf	button_block,w
		skpnz					; 	if button_block != 0
		goto	no_block
		decf	button_block,f			;		--button_block
		bsf	dflags,BLOCKED			;		dflags.BLOCKED = true
no_block	btfss	dflags,BLOCKED			;	if !dflags.BLOCKED &&
		btfss	buttons+REST_BTN,PRESSED	;	btn_0_pressed
		goto	no_rest
		bsf	dflags,RESTING			;		resting = true
		movLw	BLOCK
		movwf	button_block			;		button_block = 50
		clrf	resetcounter			;		resetcounter = 0
no_rest
;		btfss	dflags,BLOCKED			;	if !dflags.BLOCKED &&
;		btfss	buttons+MODE_BTN,PRESSED	;	btn_1_pressed
;		goto	no_mode
;		call	nextmode			;		nextmode()
;no_mode
		btfss	buttons+REST_BTN,HELD		;	if rest_btn_held
		goto	no_rest_h
		incf	resetcounter,f			;		++resetcounter
		movf	resetcounter,w			;		if resetcounter == RESET_TIME
		sublw	RESET_TIME
		skpnz
		call	reset				;			reset()
no_rest_h	btfss	dflags,BLOCKED			;	if !dflags.BLOCKED &&
		btfss	buttons+LAP_BTN,PRESSED		;	btn_1_pressed
		goto	no_lap
		call	addlap				;		laps++
no_lap		btfsc	dflags,RESTING			;	if resting
		call	pulse				;		flash_lap_display
		movf	timerpause,w			;	if timerpause == 0
		skpnz
		goto	update				;		update
		decf	timerpause,f			;	else --timerpause
notimer2	movf	pclath_temp,w 			; restore state
		movwf	PCLATH
		swapf	status_temp,w
		movwf	STATUS
		swapf	w_temp, f
		swapf	w_temp, w
		retfie
update		call	showtime
		call	showlaps
		call	update_display
		goto	notimer2

;//////////////////////////////////////////////////////////////////////
;// RESET
;//////////////////////////////////////////////////////////////////////

start		movLw	ram_start		; memclr the ram
		movwf	FSR
clear		clrf	INDF
		incf	FSR,f
		movf	FSR,w
		sublw	ram_end
		skpz
		goto	clear

		BANK1
		clrf	ANSEL			; disable analog selector thingy

		movlw	PORTA_BTNMASK		; setup IO ports
		movwf	TRISA
		clrf	TRISC

		movLw	0xff
		movwf	WPUA

		bcf	OPTION_REG,7		; switch on PORTA pullups

		BANK0
		movlw	0x07
		movwf	CMCON0			; switch off comparators

		clrf	PORTA			; all outputs low
		clrf	PORTC

		; need to wait for a bit so the 5v dc boost converter is ready....

		set7219	DISPLAY_TEST, 0x00	; setup the 7219
		set7219	SHUTDOWN, 0x01
		set7219	INTENSITY, 0x0f
		set7219	SCAN_LIMIT, 0x07
		set7219	DECODE_MODE, 0x00

		movLw	b'01001001'		; switch on timer2
		movwf	T2CON

		BANK1
		movLw	d'249'			; set timer2 reload value (1Mhz clock, so 250Khz, so 249..0 = 250 counts per reset)
		movwf	PR2

		bsf	PIE1,TMR2IE		; enable timer2 interrupts
		BANK0

		bsf	T2CON,TMR2ON		; switch on timer2
		bsf	INTCON,GIE
		bsf	INTCON,PEIE

		movLw	0x01			; init display pulse
		movwf	bdir
		movLw	0x04
		movwf	brightness

		movLw	0xff
		movwf	dp

		BANK1
		movLw	1
		movwf	EEADR
		bsf	EECON1,RD		; read the mode from EEPROM
		movf	EEDATA,w		; into W
		BANK0

		andlw	0x1
		movwf	mode

		call	set_dp			; setup decimal points

		bsf	dflags,RESTING		; resting = true

		bsf	PORTC, SHDN

		movLw	d'100'
		movwf	button_block

		movLw	d'3'
		movwf	timer+2

;//////////////////////////////////////////////////////////////////////
;// main
;//////////////////////////////////////////////////////////////////////

main		goto	main

;//////////////////////////////////////////////////////////////////////
; hex - show hex byte W on digits 5,6
;//////////////////////////////////////////////////////////////////////

hex		movwf	temp2		; save w
		movLw	0x6
		movwf	temp0
		movf	temp2,w
		andlw	0xf
		call	get_dig
		call	send7219
		movLw	0x5
		movwf	temp0
		swapf	temp2,f
		movf	temp2,w
		andlw	0xf
		call	get_dig
		call	send7219
		return

;//////////////////////////////////////////////////////////////////////
;// set_dp - setup decimal points for a mode
;//////////////////////////////////////////////////////////////////////

set_dp		call	get_dp_mask
		movwf	dp
		return

;//////////////////////////////////////////////////////////////////////
;// update_display - send the digits to the 7219 (with decimal points)
;//////////////////////////////////////////////////////////////////////

update_display	movf	dp,w
		movwf	temp3
		movLw	digits+7
		movwf	FSR
		movLw	0x8
		movwf	temp2
digit_loop	movf	temp2,w
		movwf	temp0
		movf	INDF,w
		call	get_dig
		rlf	temp3,f
		skpnc
		iorlw	0x80
		call	send7219
		decf	FSR,f
		decfsz	temp2,f
		goto	digit_loop
		return

;//////////////////////////////////////////////////////////////////////
; pulse - pulsate the LEDs
;//////////////////////////////////////////////////////////////////////

pulse		movf	bdir,w			; brightness += bdir
		addwf	brightness,f
		btfss	brightness,7		; if brightness < 0
		goto	notlo
		clrf	brightness		; brightness = 0
		movLw	1			; bdir = 1
		movwf	bdir
		goto	nothi			; else
notlo		movf	brightness,w		; if brightness <= 31
		sublw	0x1f
		skpnc
		goto	nothi
		movLw	0x1f
		movwf	brightness		; brightness = 31
		movLw	0xff			; bdir = -1
		movwf	bdir
nothi		rrf	brightness,w		; INTENSITY = brightness >> 2
		;call	intensity
		;return

;//////////////////////////////////////////////////////////////////////
;// set LED intensity
;// W = 0..31
;//////////////////////////////////////////////////////////////////////

intensity	movwf	temp1
		movLw	INTENSITY
		movwf	temp0
		movf	temp1,w
		;call	send7219
		;return

;//////////////////////////////////////////////////////////////////////
;// set7219 - send a command to the MAset7219
;// addr in temp0, data in W
;//////////////////////////////////////////////////////////////////////

send7219	bcf	PORTC,LOAD
		call	send8		; send the address in temp0
		movwf	temp0		; get the data
		call	send8		; send that too
		bsf	PORTC,LOAD
		return

; send 8 bits to the 7219

send8		clrf	temp1
		bsf	temp1,3		; temp1 = 8
bitloop		bcf	PORTC,CLK	; CLK = LOW
		bcf	PORTC,DIN	; DIN = LOW
		rlf	temp0,f		; C = temp0:7, temp <<= 1
		skpnc			; if C
		bsf	PORTC,DIN	; DIN = HIGH
		bsf	PORTC,CLK	; CLK = HIGH
		decfsz	temp1,f		; if --temp1 != 0
		goto	bitloop		; loop
		return

;//////////////////////////////////////////////////////////////////////
;// addlap : laps += 1
;//////////////////////////////////////////////////////////////////////

addlap		movLw	BLOCK
		movwf	button_block
		movLw	FREEZE
		movwf	timerpause
		call	zerotime
		call	stop_resting
		movLw	laps+4
		movwf	FSR
		movLw	d'10'
		movwf	temp0
		call	inc
		skpz
		return
		call	inc
		skpz
		return
		call	inc
		skpz
		return
		goto	inc
		;call	inc
		;return

;//////////////////////////////////////////////////////////////////////
;// stop_resting : clear resting flag and switch on lap digits
;//////////////////////////////////////////////////////////////////////

stop_resting	bcf	dflags,RESTING
		movLw	0xF
		call	intensity
		return

;//////////////////////////////////////////////////////////////////////
;// reset : reset laps and time
;//////////////////////////////////////////////////////////////////////

reset		clrf	laps+0
		clrf	laps+1
		clrf	laps+2
		clrf	laps+3

;//////////////////////////////////////////////////////////////////////
;// zerotime : Set the timer to 0
;//////////////////////////////////////////////////////////////////////

zerotime	clrf	timer+0		; reset clock
		clrf	timer+1
		clrf	timer+2
		clrf	timer+3
		clrf	timer+4
		clrf	timer+5
		return

;//////////////////////////////////////////////////////////////////////
;// showtime - update digits based on display mode
;//////////////////////////////////////////////////////////////////////

showtime	movf	mode,w
		movLw	0
		addlw	timer
		movwf	FSR
		;call	plottime
		;return
plottime
;		movf	INDF,w
;		movwf	digits+2
		incf	FSR,f
		movf	INDF,w
		movwf	digits+7
		incf	FSR,f
		movf	INDF,w
		movwf	digits+0
		incf	FSR,f
		movf	INDF,w
		movwf	digits+1
		incf	FSR,f
		movf	INDF,w
		movwf	digits+2
		incf	FSR,f
		movf	INDF,w
		movwf	digits+3
		incf	FSR,f
		return

;//////////////////////////////////////////////////////////////////////
; showlaps - update the lap digits
;//////////////////////////////////////////////////////////////////////

GNZ		macro	reg,lab
		movf	reg,f
		skpz
		goto	lab
		endm

showlaps	movf	laps+2,w
		movwf	digits+4
		movf	laps+3,w
		movwf	digits+5
		movLw	d'19'
		movwf	digits+6
		return

;//////////////////////////////////////////////////////////////////////
; do_timer - update the timer digits
;//////////////////////////////////////////////////////////////////////

do_timer	movLw	timer+6		; timer+5 is last byte (100ths), inc decrements it
		movwf	FSR		; point at it
		movLw	d'10'		; it wraps at 10
		movwf	temp0
		call	inc		; increment it?
		skpz			; wrapped?
		return			; no, that's it
		call	inc
		skpz
		return
		call	inc
		skpz
		return
		movLw	d'6'		; except for minutes, which wraps at 6
		movwf	temp0
		call	inc
		skpz
		return
		movLw	d'10'		; it wraps at 10 again
		movwf	temp0
		call	inc
		skpz
		return
		;goto	inc

inc		decf	FSR,f		; move to next digit
		incf	INDF,f		; digit += 1
		movf	temp0,w		; w = wrap
		subwf	INDF,w		; if(digit - wrap)
		btfss	STATUS,Z	; == 0
		return			; return
		clrf	INDF		; else digit = 0
		return

;//////////////////////////////////////////////////////////////////////
;// read_btns - read both the buttons
;// TODO (chs) make it read from PORTA and PORTC
;//////////////////////////////////////////////////////////////////////

read_btns	movLw	buttons
		movwf	FSR
		movLw	BTN_0_MSK
		call	chk_btn

		movLw	buttons + 2
		movwf	FSR
		movLw	BTN_1_MSK
		call	chk_btn

;		movLw	buttons + 4
;		movwf	FSR
;		movLw	BTN_2_MSK
;		call	chk_btn

		return

;//////////////////////////////////////////////////////////////////////
;// chk_btn - read a button
;// W - PORTA input mask
;// FSR[0] button flags
;// FSR[1] button debounce bits
;//////////////////////////////////////////////////////////////////////

DBMASK		equ	b'00011111'
DB_PRESS	equ	b'00001111'
DB_RELEASE	equ	b'00010000'

chk_btn		movwf	temp1			; temp1 = IO mask

		bcf	INDF,PRESSED		; PRESSED = 0
		bcf	INDF,RELEASED		; RELEASED = 0

		incf	FSR,f			; point at debounce history (next byte after flags)
		bcf	STATUS,C		; shift a zero
		rlf	INDF,f			; into the activity history

		comf	PORTA,w			; w = button status
		andwf	temp1,w			; if IO mask & status
		skpz
		bsf	INDF,0			; shift in a 1 instead

		movf	INDF,w			; check history
		decf	FSR,f			; point FSR back at flags
		andlw	DBMASK			; get recent bits we care about

		movwf	temp0
		sublw	DB_PRESS		; 01111  = a press
		skpz
		goto	notpressed
		bsf	INDF,PRESSED		; PRESSED = 1
		bsf	INDF,HELD		; HELD = 1
		return

notpressed	movf	temp0,w
		sublw	DB_RELEASE		; 10000 = a release
		btfss	STATUS,Z
		return
		bsf	INDF,RELEASED		; RELEASED = 1
		bcf	INDF,HELD		; HELD = 0
		return

;//////////////////////////////////////////////////////////////////////
; nextmode : cycle to next display mode
;//////////////////////////////////////////////////////////////////////

nextmode	movLw	BLOCK
		movwf	button_block		;		button_block = 50
		incf	mode,f			; mode += 1
		movf	mode,w
		sublw	d'3'			; if mode == 3
		skpnz
		clrf	mode			; 	mode = 0
		movf	mode,w
		call	set_dp			; setup decimals points
		movf	mode,w
		BANK1
		movwf	EEDATA			; save mode to eeprom addr 0
		movLw	1
		movwf	EEADR
		BANK0
		;goto	eeprom_write

;//////////////////////////////////////////////////////////////////////
; eeprom_write : write a byte to the EEPROM and wait for it to complete
;//////////////////////////////////////////////////////////////////////

eeprom_write	BANK0
		bcf	PIR1,EEIF
		BANK1
		bsf	EECON1, WREN
		bcf	INTCON, GIE	; Disable INTs. (not necssary here, but...)
		movlw	0x55		;
		movwf	EECON2		; Write 55h to EECON2
		movlw	0xAA
		movwf	EECON2		; Write AAh to EECON2
		bsf	EECON1, WR	; Set WR bit (begin write)
		BANK0
wait		btfsc	PIR1,EEIF
		goto	wait
		bcf	PIR1,EEIF
		BANK1
		bcf	EECON1, WREN
		BANK0
		bsf	INTCON, GIE	; Enable INTs.
		return

;//////////////////////////////////////////////////////////////////////
;//
;//
;//
;//
;// DONT PUT ANYTHING AFTER THE END!
;//
;//
;//
;//
;//////////////////////////////////////////////////////////////////////

		end


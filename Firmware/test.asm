

                LIST    p = 16F684

                include "P16F684.inc"

                ; UUUU  UNUSED          0011
                ; F     FCMEN           0       disabled
                ; I     IESO            0       disabled
                ; BB    BOREN           00      disabled
                ; D     ~CPD            1       disabled
                ; C     ~CP             1       disabled
                ; M     MCLRE           0       disabled
                ; P     ~PWRTE          0       enabled
                ; W     WDTE            0       disabled
                ; OOO   FOSC            001     XT

                        ;  UUUUFIBBDCMPWOOO
                __CONFIG b'0011000011000001'

                errorlevel -302, -231

;//////////////////////////////////////////////////////////////////////
;// CONST
;//////////////////////////////////////////////////////////////////////

;//////////////////////////////////////////////////////////////////////
;// IF THE PCB
;// YE MAKE MORE NICE,
;// CHECK THESE NUMBERS
;// SHALL THEE, THRICE!

LOAD            equ     1               ; 7219 on PORT C
CLK             equ     2
DIN             equ     5

LED             equ     3               ; SHDN on the 3400

;//////////////////////////////////////////////////////////////////////
;// RAM
;//////////////////////////////////////////////////////////////////////

                UDATA   0x20

ram_start       res     0

temp0           res     1
temp1           res     1
temp2           res     1
temp3           res     1

timer           res     6

pclath_temp     res     1

ram_end         res     0

                UDATA_SHR

w_temp          res     1               ;  W save in shared bank ram: banksel=? in irq and have to save W before selecting bank 0
status_temp     res     1

;//////////////////////////////////////////////////////////////////////
;// MACROS
;//////////////////////////////////////////////////////////////////////

BANK0           MACRO                   ; assume RP1 = 0...
                bcf     STATUS, RP0
                ENDM

BANK1           MACRO
                bsf     STATUS, RP0     ; assume RP1 = 0...
                ENDM

SWAPWF          macro reg
                xorwf reg,f
                xorwf reg,w
                xorwf reg,f
                endm

set7219         MACRO   addr,val
                movLw   addr
                movwf   temp0
                movLw   val
                call    send7219
                ENDM

;//////////////////////////////////////////////////////////////////////
;// reset/irq vectors
;//////////////////////////////////////////////////////////////////////

zero            CODE    0

                goto    start

                nop
                nop
                nop

                goto    isr

;//////////////////////////////////////////////////////////////////////
;// some things in low mem because PCL/PCLATH 8 bit shenanigans
;//////////////////////////////////////////////////////////////////////


;//////////////////////////////////////////////////////////////////////
;// ISR
;//////////////////////////////////////////////////////////////////////

isr             movwf   w_temp                          ; save state in shared ram
                swapf   STATUS, w                       ; get status in W
                clrf    STATUS                          ; now we're in bank0
                movwf   status_temp                     ; save status in bank0
                movf    PCLATH, w
                movwf   pclath_temp                     ; save pclath in bank0
                clrf    PCLATH                          ; clear high bits of PC before any gotos, calls etc
                btfss   PIR1,TMR2IF                     ; if timer2 irq
                goto    notimer2
                bcf     PIR1,TMR2IF                     ;       clear it
                call    do_timer                        ;               increment_timer()
notimer2        movf    pclath_temp,w                   ; restore state
                movwf   PCLATH
                swapf   status_temp,w
                movwf   STATUS
                swapf   w_temp, f
                swapf   w_temp, w
                retfie

;//////////////////////////////////////////////////////////////////////
;// RESET
;//////////////////////////////////////////////////////////////////////

start           movLw   ram_start               ; memclr the ram
                movwf   FSR
clear           clrf    INDF
                incf    FSR,f
                movf    FSR,w
                sublw   ram_end
                skpz
                goto    clear

                BANK1
                clrf    ANSEL                   ; disable analog selector thingy

                clrf    TRISA
                clrf    TRISC

                movLw   0xff
                movwf   WPUA

                bcf     OPTION_REG,7            ; switch on PORTA pullups

                BANK0
                movlw   0x07
                movwf   CMCON0                  ; switch off comparators

                clrf    PORTA                   ; all outputs low
                clrf    PORTC

                ; need to wait for a bit so the 5v dc boost converter is ready....

                movLw   b'01001001'             ; switch on timer2
                movwf   T2CON

                BANK1
                movLw   d'249'                  ; set timer2 reload value (1Mhz clock, so 250Khz, so 249..0 = 250 counts per reset)
                movwf   PR2

                bsf     PIE1,TMR2IE             ; enable timer2 interrupts
                BANK0

                bsf     T2CON,TMR2ON            ; switch on timer2
                bsf     INTCON,GIE
                bsf     INTCON,PEIE

;//////////////////////////////////////////////////////////////////////
;// main
;//////////////////////////////////////////////////////////////////////

main
                bcf     PORTC, 4
                BANK1
                bcf     TRISC, 4
                BANK0

                btfss   timer+3,0
                goto    off
                bcf     PORTC, LED
                goto    done1
off             bsf     PORTC, LED
done1
                goto    main

;//////////////////////////////////////////////////////////////////////
;// zerotime : Set the timer to 0
;//////////////////////////////////////////////////////////////////////

zerotime        clrf    timer+0         ; reset clock
                clrf    timer+1
                clrf    timer+2
                clrf    timer+3
                clrf    timer+4
                clrf    timer+5
                return

;//////////////////////////////////////////////////////////////////////
; do_timer - update the timer digits
;//////////////////////////////////////////////////////////////////////

do_timer        movLw   timer+6         ; timer+5 is last byte (100ths), inc decrements it
                movwf   FSR             ; point at it
                movLw   d'10'           ; it wraps at 10
                movwf   temp0
                call    inc             ; increment it?
                skpz                    ; wrapped?
                return                  ; no, that's it
                call    inc
                skpz
                return
                call    inc
                skpz
                return
                movLw   d'6'            ; except for minutes, which wraps at 6
                movwf   temp0
                call    inc
                skpz
                return
                movLw   d'10'           ; it wraps at 10 again
                movwf   temp0
                call    inc
                skpz
                return
                ;goto   inc

inc             decf    FSR,f           ; move to next digit
                incf    INDF,f          ; digit += 1
                movf    temp0,w         ; w = wrap
                subwf   INDF,w          ; if(digit - wrap)
                btfss   STATUS,Z        ; == 0
                return                  ; return
                clrf    INDF            ; else digit = 0
                return

;//////////////////////////////////////////////////////////////////////
;//
;//
;//
;//
;// DONT PUT ANYTHING AFTER THE END!
;//
;//
;//
;//
;//////////////////////////////////////////////////////////////////////

                end

